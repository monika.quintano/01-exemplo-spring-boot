package br.com.itau.marketplace.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.marketplace.models.Produto;
import br.com.itau.marketplace.models.Usuario;
import br.com.itau.marketplace.repositories.ProdutoRepository;
import br.com.itau.marketplace.repositories.UsuarioRepository;

@Service
public class ProdutoService {

	@Autowired
	ProdutoRepository produtoRepository;
	
	@Autowired UsuarioRepository usuarioRepository;
	
	public Iterable<Produto> buscarProdutos(){
		return produtoRepository.findAll();
	}
	
	public Produto buscarProdutoPorId(int id) {
		Optional<Produto> produtoOptional = produtoRepository.findById(id);
		
		if(produtoOptional.isPresent()) {
			return produtoOptional.get();
		}
		
		return null;
	}
	
	public boolean inserir(Produto produto) {
		Optional<Usuario> usuarioOptional = usuarioRepository.findById(produto.getProprietario().getId());
		
		if(usuarioOptional.isPresent()) {
			produtoRepository.save(produto);
			return true;
		}
		
		return false;
	}
}
